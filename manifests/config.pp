# Up1 Configuration
class up1::config inherits up1 {

  $server = "${default_path}/server.conf"
  $config = "${default_path}/config.js"

  file { $server:
      ensure  => file,
      owner   => $up1::default_user,
      group   => $up1::default_group,
      mode    => '0644',
      content => template('up1/server.conf.erb'),
      notify  => Service['up1']
  }

  file { $config:
      ensure  => file,
      owner   => $up1::default_user,
      group   => $up1::default_group,
      mode    => '0644',
      content => template('up1/config.js.erb'),
      notify  => Service['up1']
  }

}
